package com.example.fizzbuzz.api;

import com.example.fizzbuzz.dto.FizzBuzzRequest;
import com.example.fizzbuzz.service.FizzBuzzService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@Validated
@RequestMapping("/api")
public class FizzBuzzApi {

    private final FizzBuzzService fizzBuzzService;

    public FizzBuzzApi(FizzBuzzService fizzBuzzService) {
        this.fizzBuzzService = fizzBuzzService;
    }

    @GetMapping("/fizzbuzz")
    public List<String> fizzBuzz(@RequestParam @Min(1) int int1,
                                 @RequestParam @Min(1) int int2,
                                 @RequestParam @Min(1) int limit,
                                 @RequestParam @NotBlank String string1,
                                 @RequestParam @NotBlank String string2) {

        return fizzBuzzService.mapFizzBuzz(new FizzBuzzRequest(int1, int2, limit, string1, string2));
    }
}
