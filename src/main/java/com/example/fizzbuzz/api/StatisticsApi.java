package com.example.fizzbuzz.api;

import com.example.fizzbuzz.dto.FizzBuzzRequest;
import com.example.fizzbuzz.service.StatisticService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/statistics")
public class StatisticsApi {

    private final StatisticService statisticService;

    public StatisticsApi(StatisticService statisticService) {
        this.statisticService = statisticService;
    }

    @GetMapping("most-used")
    public Optional<Map.Entry<FizzBuzzRequest, Integer>> getMostUseRequest() {
        return statisticService.getMostUsedRequest();
    }
}
