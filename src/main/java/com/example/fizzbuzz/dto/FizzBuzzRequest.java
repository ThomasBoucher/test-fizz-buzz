package com.example.fizzbuzz.dto;

public record FizzBuzzRequest(int int1, int int2, int limit, String string1, String string2) {
}
