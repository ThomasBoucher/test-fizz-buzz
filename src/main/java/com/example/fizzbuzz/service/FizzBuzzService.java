package com.example.fizzbuzz.service;

import com.example.fizzbuzz.dto.FizzBuzzRequest;
import com.example.fizzbuzz.util.NumberUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.IntStream;

@Service
public class FizzBuzzService {

    private final StatisticService statisticService;

    public FizzBuzzService(StatisticService statisticService) {
        this.statisticService = statisticService;
    }

    public List<String> mapFizzBuzz(FizzBuzzRequest requestDto) {
        statisticService.addRequestDataToStatistics(requestDto);
        return IntStream.rangeClosed(1, requestDto.limit())
                .mapToObj(i -> getFizzBuzz(i, requestDto.int1(), requestDto.int2(), requestDto.string1(), requestDto.string2()))
                .toList();
    }

    public String getFizzBuzz(int i, int firstDivider, int secondDivider, String firstWord, String secondWord) {
        if (!NumberUtil.isMultipleOf(i, firstDivider) && !NumberUtil.isMultipleOf(i, secondDivider)) {
            return String.valueOf(i);
        } else {
            return String.format("%s%s",
                    NumberUtil.isMultipleOf(i, firstDivider) ? firstWord : "",
                    NumberUtil.isMultipleOf(i, secondDivider) ? secondWord : "");
        }
    }
}
