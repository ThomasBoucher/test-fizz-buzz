package com.example.fizzbuzz.service;

import com.example.fizzbuzz.dto.FizzBuzzRequest;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class StatisticService {

    Map<FizzBuzzRequest, Integer> stats = new ConcurrentHashMap<>();

    /**
     * Adds request data to statistics
     * Method is async to optimise response performance
     *
     * @param fizzBuzzRequest
     */
    @Async
    public void addRequestDataToStatistics(FizzBuzzRequest fizzBuzzRequest) {
        stats.merge(fizzBuzzRequest, 1, Integer::sum);
    }

    public Optional<Map.Entry<FizzBuzzRequest, Integer>> getMostUsedRequest() {
        return stats.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue());
    }
}
