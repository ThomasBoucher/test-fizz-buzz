package com.example.fizzbuzz.util;

public class NumberUtil {

    public static boolean isMultipleOf(int a, int b) {
        return a % b == 0;
    }
}
