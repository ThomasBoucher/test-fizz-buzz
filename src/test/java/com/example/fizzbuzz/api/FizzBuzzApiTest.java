package com.example.fizzbuzz.api;

import org.junit.jupiter.api.Test;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class FizzBuzzApiTest extends ApiTest {

    @Test
    void fizzBuzzEndpointTest() throws Exception {
        this.mockMvc.perform(get("/api/fizzbuzz")
                        .param("int1", "3")
                        .param("int2", "5")
                        .param("limit", "15")
                        .param("string1", "fizz")
                        .param("string2", "buzz"))
                .andExpect(status().isOk())
                .andDo(document("fizz-buzz",
                        requestParameters(
                                parameterWithName("int1").description(""),
                                parameterWithName("int2").description(""),
                                parameterWithName("limit").description(""),
                                parameterWithName("string1").description(""),
                                parameterWithName("string2").description("")),
                        responseFields(
                                fieldWithPath("[]").description("The list")
                        )));
    }
}