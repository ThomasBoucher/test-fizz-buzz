package com.example.fizzbuzz.api;

import com.example.fizzbuzz.dto.FizzBuzzRequest;
import com.example.fizzbuzz.service.StatisticService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.AbstractMap;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class StatisticsApiTest extends ApiTest {

    @SpyBean
    StatisticService statisticService;

    @Test
    void getStatisticsEndpoint() throws Exception {
        AbstractMap.SimpleEntry<FizzBuzzRequest, Integer> requestDataIntegerSimpleEntry = new AbstractMap.SimpleEntry<>(new FizzBuzzRequest(3, 5, 100, "fizz", "buzz"), 15);

        when(statisticService.getMostUsedRequest()).thenReturn(Optional.of(requestDataIntegerSimpleEntry));

        this.mockMvc.perform(get("/api/statistics/most-used"))
                .andExpect(status().isOk())
                .andDo(print());
    }
}