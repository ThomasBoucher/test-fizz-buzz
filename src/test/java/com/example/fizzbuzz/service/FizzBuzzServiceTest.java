package com.example.fizzbuzz.service;

import com.example.fizzbuzz.dto.FizzBuzzRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
class FizzBuzzServiceTest {

    @Autowired
    FizzBuzzService fizzBuzzService;

    @Test
    void assertThatWeCanGenerateList() {
        FizzBuzzRequest requestDto = new FizzBuzzRequest(3, 5, 15, "fizz", "buzz");
        List<String> list = fizzBuzzService.mapFizzBuzz(requestDto);

        assertFalse(list.isEmpty());
        assertEquals(list.size(), requestDto.limit());
    }

    @Test
    void assertThatWeCanGetRightWord() {
        final String firstWord = "firstWord";
        final String secondWord = "secondWord";

        assertEquals(fizzBuzzService.getFizzBuzz(2, 2, 9, firstWord, secondWord), firstWord);
        assertEquals(fizzBuzzService.getFizzBuzz(4, 2, 9, firstWord, secondWord), firstWord);
        assertEquals(fizzBuzzService.getFizzBuzz(4, 9, 2, firstWord, secondWord), secondWord);
        assertEquals(fizzBuzzService.getFizzBuzz(9, 2, 9, firstWord, secondWord), secondWord);
        assertEquals(fizzBuzzService.getFizzBuzz(9, 9, 2, firstWord, secondWord), firstWord);
        assertEquals(fizzBuzzService.getFizzBuzz(18, 2, 9, firstWord, secondWord), firstWord + secondWord);
        assertEquals(fizzBuzzService.getFizzBuzz(36, 2, 9, firstWord, secondWord), firstWord + secondWord);
        assertEquals(fizzBuzzService.getFizzBuzz(36, 9, 2, firstWord, secondWord), firstWord + secondWord);
        assertEquals(fizzBuzzService.getFizzBuzz(36, 9, 2, secondWord, firstWord), secondWord + firstWord);
        assertEquals(fizzBuzzService.getFizzBuzz(1, 2, 9, firstWord, secondWord), String.valueOf(1));
        assertEquals(fizzBuzzService.getFizzBuzz(3, 2, 9, firstWord, secondWord), String.valueOf(3));
    }
}