package com.example.fizzbuzz.service;

import com.example.fizzbuzz.dto.FizzBuzzRequest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

@SpringBootTest
class StatisticServiceTest {

    @SpyBean
    StatisticService statisticService;

    @Test
    void addRequestDataToStatistics() {
        statisticService.addRequestDataToStatistics(new FizzBuzzRequest(3, 5, 15, "fizz", "buzz"));
        statisticService.addRequestDataToStatistics(new FizzBuzzRequest(3, 5, 15, "fizz", "buzz"));
        statisticService.addRequestDataToStatistics(new FizzBuzzRequest(3, 5, 15, "fizz", "buzz"));
        statisticService.addRequestDataToStatistics(new FizzBuzzRequest(3, 5, 15, "fizz", "buzz"));
        statisticService.addRequestDataToStatistics(new FizzBuzzRequest(3, 5, 15, "buzz", "fizz"));

        verify(statisticService, timeout(500).times(5)).addRequestDataToStatistics(any(FizzBuzzRequest.class));

        Map<FizzBuzzRequest, Integer> stats = (Map<FizzBuzzRequest, Integer>) ReflectionTestUtils.getField(statisticService, "stats");
        assertEquals(2, stats.size());
    }

    @Test
    void getMostUseRequest() {
        statisticService.addRequestDataToStatistics(new FizzBuzzRequest(3, 5, 15, "fizz", "buzz"));
        statisticService.addRequestDataToStatistics(new FizzBuzzRequest(3, 5, 15, "fizz", "buzz"));
        statisticService.addRequestDataToStatistics(new FizzBuzzRequest(3, 5, 0, "fizz", "buzz"));
        statisticService.addRequestDataToStatistics(new FizzBuzzRequest(2, 6, 15, "fizz", "buzz"));
        statisticService.addRequestDataToStatistics(new FizzBuzzRequest(2, 7, 150, "buzz", "fizz"));

        verify(statisticService, timeout(500).times(5)).addRequestDataToStatistics(any(FizzBuzzRequest.class));

        Optional<Map.Entry<FizzBuzzRequest, Integer>> mostUsedRequest = statisticService.getMostUsedRequest();
        assertTrue(mostUsedRequest.isPresent());
        assertEquals(mostUsedRequest.get().getKey(), new FizzBuzzRequest(3, 5, 15, "fizz", "buzz"));
        assertEquals(mostUsedRequest.get().getValue(), 2);
    }

    @Test
    void getMostUseRequestEmpty() {
        Optional<Map.Entry<FizzBuzzRequest, Integer>> mostUsedRequest = statisticService.getMostUsedRequest();
        assertTrue(mostUsedRequest.isEmpty());
    }
}