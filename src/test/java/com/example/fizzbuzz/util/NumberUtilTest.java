package com.example.fizzbuzz.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NumberUtilTest {

    @Test
    void isMultiple() {
        assertTrue(NumberUtil.isMultipleOf(1, 1));
        assertTrue(NumberUtil.isMultipleOf(10, 2));
        assertTrue(NumberUtil.isMultipleOf(6, 3));
        assertTrue(NumberUtil.isMultipleOf(6, 2));
    }

    @Test
    void isNotMultiple() {
        assertFalse(NumberUtil.isMultipleOf(3, 2));
        assertFalse(NumberUtil.isMultipleOf(31, 2));
        assertFalse(NumberUtil.isMultipleOf(7, 2));
        assertFalse(NumberUtil.isMultipleOf(8, 7));
    }

}